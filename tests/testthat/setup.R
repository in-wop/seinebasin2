library(seinebasin2)
cfg <- loadConfig()
if (Sys.getenv("CI") == "") {
    # Local configuration
    cfg$data$cloud <- FALSE
    cfg$data$path <- "C:/DocDD/nextcloud.inrae.fr/mo-geau/PRJ_2019-2023_IN-WOP/2_Data/seinebasin2"
}
