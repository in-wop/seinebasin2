Package: seinebasin2
Type: Package
Title: Seine River Basin Integrated Modeling with airGRiwrm
Version: 0.3.0.9000
Authors@R: c(
    person("David", "Dorchies", role = c("aut", "cre"), comment = c(ORCID = "0000-0002-6595-7984"), email = "david.dorchies@inrae.fr"),
    person("Florian", "Ricquier", role = c("aut"), email = "florian.ricquier@inrae.fr"),
    person("Olivier", "Delaigue", role = c("ctb"), comment = c(ORCID = "0000-0002-7668-8468"), email = "airGR@inrae.fr"),
    person("Guillaume", "Thirel", role = c("ctb"), comment = c(ORCID = "0000-0002-1444-1830")),
    person("Laura", "Nunez Torres", role = c("ctb"))
    )
Description: Calibration and modeling of a semi-distributed model of the Seine River accounting of the reservoirs and usages.
URL: https://in-wop.g-eau.fr/seinebasin2/package/index.html
BugReports: https://forgemia.inra.fr/in-wop/seinebasin2/-/issues/
License: AGPL (>= 3)
Encoding: UTF-8
LazyData: true
Suggests:
    bookdown,
    rmarkdown,
    knitr,
    prettymapr,
    raster,
    glue,
    purrr,
    testthat (>= 3.0.0),
    tidyr,
    tidyquant,
    methods,
    tmap,
    mapsf,
    png,
    logger,
    extRemes,
    kableExtra
VignetteBuilder: knitr
Depends:
    R (>= 2.10),
    airGRiwrm (> 0.6.1),
Imports:
    airGR,
    rvgest,
    config,
    xts,
    zoo,
    dplyr,
    fairify,
    magrittr,
    lubridate,
    stats,
    ncdf4,
    readr,
    sf,
    sp,
    ggplot2,
    ggspatial,
    ggrepel,
    plotrix,
    RColorBrewer,
    leaflet,
    leaflet.extras,
    leafpop,
    utils,
    urltools,
    htmltools,
    httr
Roxygen: list(markdown = TRUE)
RoxygenNote: 7.3.1
Config/testthat/edition: 3
remotes:
    git::https://github.com/inrae/airGRiwrm.git@dev,
    git::https://forgemia.inra.fr/in-wop/rvgest,
    git::https://forgemia.inra.fr/umr-g-eau/fairify.git,
