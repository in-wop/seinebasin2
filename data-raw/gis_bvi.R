#' Record GIS shape file of intermediate basin edges

# Local config
library(seinebasin2)
cfg <- loadConfig("bookdown/config.yml")

gis_bvi <- rgdal::readOGR(dsn = getDataPath(cfg$hydroclim$path, "GIS", "bvi_shape", cfg = cfg),
                      layer = "143_contours_BV",
                      encoding = "UTF-8",
                      use_iconv = TRUE)

usethis::use_data(gis_bvi, overwrite = TRUE)
