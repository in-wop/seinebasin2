#' Record GIS shape file of intermediate basin edges

# Local config
library(seinebasin2)
cfg <- loadConfig()

gis_bvi_V2 <- rgdal::readOGR(dsn = getDataPath(cfg$hydroclim$path, "GIS", "bvi_shape", cfg = cfg),
                      layer = "152_contours_BV",
                      encoding = "UTF-8",
                      use_iconv = TRUE)

usethis::use_data(gis_bvi_V2, overwrite = TRUE)
