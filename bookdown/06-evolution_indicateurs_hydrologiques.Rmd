# Evolution des indicateurs hydrologiques

```{r setup, include=FALSE}
library(seinebasin2)
cfg <- loadConfig()
knitr::opts_chunk$set(
  fig.width = 8,
  fig.asp = 0.4
)
```

L'évolution est représentée sous la forme du rapport entre l'indicateur calculé sur la période future et celui calculé sur la période de référence.

La période de référence correspond à la période 1976-2005 et la période future choisie ici correspond à la période "fin de siècle" 2071-2100.

Les indicateurs sont calculés séparément pour les deux scénarios d'émission RCP 4.5 et RCP 8.5 et une synthèse des différents couples GCM/RCM est réalisée.

```{r}
rcps <- cfg$hydroclim$drias$rcp[-1]
scenarios <- gsub("/", "_", cfg$hydroclim$drias$scenarios)
scenariosX <- rep(scenarios, length(rcps))
rcpsX <- rep(rcps, each = length(scenarios))
```

## Listes des indicateurs

### Indicateurs mensuels

Pour les données climatiques&nbsp;:

- Cumul pluviométrie (mm)
- Moyenne températures (°C)
- Cumul ETP (mm)

```{r}
# Chargement des données climatiques moyennes mensuelles
readTsvMatrix <- function(path) {
    df <- read.csv(path, sep = "\t")
    m <- as.matrix(df[,-1])
    rownames(m) <- df[, 1]
    m
}

# Pour les données DRIAS
readScenarioClimatic <- function(rcp, scenario, varName) {
    lapply(cfg$hydroclim$drias$periods,
           function(period) {
               file <- paste0(
                   varName, "_monthly_",
                   rcp,"-",scenario, "_",
                   substr(period[1], 1, 4), "-",
                   substr(period[2], 1, 4), ".tsv"
               )
               readTsvMatrix(getDataPath(cfg$hydroclim$path, "Analyses", file))
           })
}

readAllClimatic <- function(varName) {
    l <- mapply(rcp = rcpsX,
                scenario = scenariosX,
                varName = varName,
                readScenarioClimatic,
                SIMPLIFY = FALSE)
    names(l) <- paste(rcpsX, scenariosX, sep = " - ")
    return(l)
}

P_month <- readAllClimatic("P")
T_month <- readAllClimatic("T")
E_month <- readAllClimatic("E")

# Pour les obs
Pobs_month <- readTsvMatrix(
    getDataPath(cfg$hydroclim$path, "Analyses","P_monthly_obs_1976-2005.tsv")
)
Tobs_month <- readTsvMatrix(
    getDataPath(cfg$hydroclim$path, "Analyses","T_monthly_obs_1976-2005.tsv")
)
Eobs_month <- readTsvMatrix(
    getDataPath(cfg$hydroclim$path, "Analyses","E_monthly_obs_1976-2005.tsv")
)
```

Pour les débits&nbsp;:

- Débit moyen
- Débit moyen sec de période de retour 5 ans

Ces données ont été calculées et enregistrées lors de la simulation des débits.

```{r}
# Chargement résultats simulations forçages DRIAS
loadIndicators <- function(rcp, scenario, indicator) {
    periods <- names(cfg$hydroclim$drias$periods)
    names(periods) <- periods
    lapply(periods, function(period) {
        file <- paste0(indicator, "_",
                       substr(cfg$hydroclim$drias$periods[[period]][1], 1, 4),
                       "-",
                       substr(cfg$hydroclim$drias$periods[[period]][2], 1, 4),
                       ".tsv")
        path <- getDataPath(
            cfg$Qnat$path,
            rcp,
            "data",
            scenario,
            file,
            cfg = cfg
        )
        readTsvMatrix(path)
    })
}

loadAllIndicators <- function(indicator) {
    ind <- mapply(
        rcp = rcpsX,
        scenario = scenariosX,
        indicator = indicator,
        loadIndicators,
        SIMPLIFY = FALSE
    )
    names(ind) <- paste(rcpsX, scenariosX, sep = " - ")
    return(ind)
}

Q_month <- loadAllIndicators("Q_monthly")
Q_month5 <- loadAllIndicators("Q_monthly_5years")
```

```{r}
# Chargement résultats simulations forçages climat observé SAFRAN
historiQ <- list(
    obs = list(path = file.path(cfg$calibration$path, "Qobs"),
               period = c(cfg$calibration$date$start, cfg$calibration$date$end)),
    sim = list(path = file.path(cfg$calibration$path, "Qsim"),
               period = c(cfg$calibration$date$start, cfg$calibration$date$end)),
    nat = list(path = file.path(cfg$Qnat$path, "historical/data"),
               period = cfg$hydroclim$drias$periods$ref)
)

loadHistoriQ <- function(l, indicator) {
    file <- paste0(indicator, "_",
                   substr(l$period[1], 1, 4),
                   "-",
                   substr(l$period[2], 1, 4),
                   ".tsv")
    path <- getDataPath(l$path, file, cfg = cfg)
    readTsvMatrix(path)
}
Qhist_month <- lapply(historiQ, loadHistoriQ, indicator = "Q_monthly")
Qhist_month5 <- lapply(historiQ, loadHistoriQ, indicator = "Q_monthly_5years")
Qhist_indicators <- lapply(historiQ, loadHistoriQ, indicator = "Q_indicators")
```

### Indicateurs synthétiques

Pour les étiages&nbsp;: VCN10, VCN30 et QMNA pour les périodes de retour 2 ans, 5 ans et 10 ans.

Pour les crues&nbsp;: QJXA pour les périodes de retour 2 ans, 10 ans, 20 ans.

Pour les faibles/forts débits&nbsp;: les quantiles 95% et 10% de débits journaliers

```{r}
Q_indicators <- loadAllIndicators("Q_indicators")
```


## Résultats calculés pour chaque indicateur

### Données climatiques moyennes mensuelles

```{r, fig.cap="Précipitations moyennes mensuelles du bassin de la Seine à Paris sur la période 1976-2005"}
plot_monthly_mean("rcp8.5", "ref", "H5920010", P_month, list(obs = Pobs_month), "Precipitation (mm)")
```

```{r, fig.cap="Précipitations moyennes mensuelles du bassin de la Seine à Paris sur la période 2071-2100"}
plot_monthly_mean("rcp8.5", "end", "H5920010", P_month, list(obs = Pobs_month), "Precipitation (mm)")
```

```{r, fig.cap="Températures moyennes mensuelles du bassin de la Seine à Paris sur la période 1976-2005"}
plot_monthly_mean("rcp8.5", "ref", "H5920010", T_month, list(obs = Tobs_month), "Temperature (°C)")
```

```{r, fig.cap="Températures moyennes mensuelles du bassin de la Seine à Paris sur la période 2071-2100"}
plot_monthly_mean("rcp8.5", "end", "H5920010", T_month, list(obs = Tobs_month), "Temperature (°C)")
```


### Débits moyens mensuels

Débits moyens mensuels à Paris sur la période de référence:

```{r, fig.asp = 0.7}
plot_monthly_mean("rcp8.5", "ref", "H5920010", Q_month, Qhist_month)
```

Débits moyens à Paris sur la période 2071-2100 avec le scénario d'émission RCP8.5:

```{r, fig.asp = 0.7}
plot_monthly_mean("rcp8.5", "end", "H5920010", Q_month, Qhist_month)
```

```{r, eval=cfg$data$write_results}
# Sauvegarde des plots des régimes dans le cloud
Phist_month <- list(obs = Pobs_month)
Thist_month <- list(obs = Tobs_month)
Ehist_month <- list(obs = Eobs_month)

saveMonthlyPlot <- function(parameter, rcp, period, station, y_label) {
    drias <- get(paste0(parameter, "_month"))
    hist <- get(paste0(parameter, "hist_month"))
    path <- getDataPath(cfg$Qnat$path, rcp, "plots", paste0(parameter, "_month"), cfg = cfg)
    dir.create(path, showWarnings = FALSE)
    file <- file.path(path, 
                      paste(parameter, "monthly", 
                            station, 
                            iconv_filename(griwrm$nom[griwrm$id == station]),
                            paste(lubridate::year(cfg$hydroclim$drias$periods[[period]]), collapse = "-"),
                            sep = "_"))
    file <- paste0(file, ".png")
    p <- plot_monthly_mean(rcp, period, station, drias, hist, y_label)
    ggplot2::ggsave(file, p, width = 7, height = 4, dpi = 150)
    file
}

y_labels <- c("P" = "Precipitation (mm)",
              "T" = "Temperature (°C)",
              "E" = "Evaporation (mm)",
              "Q" = "Flow (m³/s)"
)
                

if (cfg$data$write_results) 
    for (parameter in c("P", "E", "T", "Q"))
        for (rcp in cfg$hydroclim$drias$rcp[-1])
            for (period in names(cfg$hydroclim$drias$period))
                for (station in griwrm$id)
                    saveMonthlyPlot(parameter, rcp, period, station, y_labels[parameter])
```


Les données utilisées en entrées sont&nbsp;:

- données observées sur la période d'observation
- données simulées sur la période d'observation
- Rapport entre les données simulées sur la période future et la période de référence pour un couple scénario/modèle climatique

Les tableaux de synthèse fournissent la valeur minimale, médiane et maximale des évolutions parmi les couples scénario/modèle climatiques.

```{r}
calcDelta <- function(ind, rcp, period, delta = "*") {
    if (!delta %in% c("*", "+")) stop("`delta`should be equal to \"*\" or \"+\"")
    scenarios <- names(ind)
    names(scenarios) <- scenarios
    ind <- lapply(scenarios, function(x) {
        if (grepl(rcp, x, fixed = TRUE)) {

            ind[[x]]
        } else {
            NULL
        }
    })
    ind <- ind[!sapply(ind,is.null)]
    deltas <- lapply(names(ind), function(scenario) {
        dfPer <- ind[[scenario]][[period]]
        dfRef <- ind[[scenario]]$ref
        if (delta == "*") {
            m <- (dfPer - dfRef) / dfRef * 100
        } else {
            m <- dfPer - dfRef
        }
    })
    list(
        min = do.call(pmin, deltas),
        med = do.call(pmedian, deltas),
        max = do.call(pmax, deltas)
    )
}
tableDeltaStation <- function(station, delta, ind_hist = NULL) {
    if (!is.null(ind_hist)) delta <- c(ind_hist, delta)
    l <- lapply(delta, function(m) {
        m[station, ]
    })
    do.call(cbind, l)
}
```

Exemple pour les pluies moyennes mensuelles pour les 6 premières stations:

```{r}
deltaPM <- calcDelta(P_month, rcp = "rcp8.5", period = "end")
lapply(deltaPM, function(x) t(head(x)))
```

Exemple pour la température moyenne mensuelle à Paris:

```{r}
deltaTM <- calcDelta(T_month, rcp = "rcp8.5", period = "end", delta = "+")
knitr::kable(tableDeltaStation("H5920010", deltaTM, Thist_month), digits = 1)
```

**N.B.:** les données climatiques correspondent ici au données moyennes du sous-bassin versant et pas le bassin versant entier. Il faudrait agréger les données des bassins amont pour avoir une moyenne du bassin pour chaque station.

Exemple sur le débit mensuel moyen à Paris

```{r}
deltaQM <-  calcDelta(Q_month, rcp = "rcp8.5", period = "end")
knitr::kable(tableDeltaStation("H5920010", deltaQM, Qhist_month), digits = 1)
```

Exemple sur tous les indicateurs hydrologiques à Paris:

```{r}
delta_indicators <- calcDelta(Q_indicators, rcp = "rcp8.5", period = "end")
knitr::kable(tableDeltaStation("H5920010", delta_indicators, Qhist_indicators), digits = 1)
```

## Cartes d'évolutions

```{r, fig.asp = 1}
plot_map_delta <- function(r, title,
                           breaks = c(-75, -50, -25, -10, -5,
                                      5, 10, 25, 50, 150)) {
    plot_seine_map(r, breaks, title)
}
```

```{r, fig.asp = 1}
plot_map_delta(delta_indicators$med[, "QA"], "QA - évolution RCP8.5 scénario médian (%)")
```

```{r, fig.asp = 1}
plot_map_delta(delta_indicators$med[, "QMNA5"], "QMNA5 - évolution RCP8.5 scénario médian (%)")
```

```{r, fig.asp = 1}
plot_map_delta(delta_indicators$med[, "QJXA10"], "QJXA10 - évolution RCP8.5 scénario médian (%)")
```


```{r}
# Sauvegarde des tableaux dans le cloud
saveDeltaStation <- function(station, delta, hist, indicator_name, rcp, period) {
    df <- as.data.frame(tableDeltaStation(station["id"], delta, hist))
    df <- cbind(indicator = row.names(df), df)
    path <- path <- getDataPath(cfg$Qnat$path, rcp, "data", "delta", indicator_name, cfg = cfg)
    dir.create(path, showWarnings = FALSE, recursive = TRUE)
    file <- file.path(path, 
                      paste(indicator_name, 
                            station["id"], 
                            iconv_filename(station["nom"]),
                            paste(lubridate::year(cfg$hydroclim$drias$periods[[period]]), collapse = "-"),
                            sep = "_"))
    file <- paste0(file, ".tsv")
    readr::write_tsv(df, file)
}
saveDeltaIndicators <- function(parameter, rcp, period) {
    indicator_name <- paste(parameter, collapse = "_")
    indicator <- get(indicator_name)
    hist <- get(paste0(parameter[1], "hist_", parameter[2]))
    delta <- calcDelta(indicator, rcp = rcp, period = period)
    # Save raw data
    apply(griwrm, 1, 
          saveDeltaStation, 
          delta = delta, 
          hist = hist, 
          indicator_name = indicator_name, 
          rcp = rcp, 
          period = period)
    # Save maps
    trends <- c(min = "minimum", med = "médian", max = "maximum")
    if (indicator_name == "Q_indicators") {
        for (hydro in colnames(indicator[[1]][[1]])) 
            for (trend in names(trends)) {
                path <- path <- getDataPath(cfg$Qnat$path, rcp, "maps", cfg = cfg)
                dir.create(path, showWarnings = FALSE, recursive = TRUE)
                file <- file.path(path, 
                                  paste(hydro, trend,
                                        paste(lubridate::year(cfg$hydroclim$drias$periods[[period]]), 
                                              collapse = "-"),
                                        sep = "_"))
                file <- paste0(file, ".png")
                message("Creating ", file)
                png(file, width = 6, height = 6, units = "in", res = 150)
                plot_map_delta(
                    delta[[trend]][, hydro], 
                    paste(hydro, "- évolution", rcp, "scénario", trends[trend], "(%)")
                )
                dev.off()
            }
    }
}

for (parameter in list(c("P", "month"), c("T", "month"), c("E", "month"), c("Q", "month"), c("Q", "indicators")))
    for (rcp in cfg$hydroclim$drias$rcp[-1])
            for (period in names(cfg$hydroclim$drias$period[-1]))
                saveDeltaIndicators(parameter, rcp, period)

```

