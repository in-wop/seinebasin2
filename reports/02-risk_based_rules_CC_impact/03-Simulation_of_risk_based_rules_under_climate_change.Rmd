# Simulation of risk based rules under climate change {#Simulation_of_risk_based_rules_under_climate_change}

```{r 03-setup, include = FALSE, file = 'setup.R'}
```

```{r, include=FALSE}
library(xts)
library(future)
library(future.apply)
```

```{r, eval = TRUE, message=FALSE}
cfg <- loadConfig()
```

```{r}
rcps <- cfg$hydroclim$drias$rcp[-1]
scenarios <- gsub("/", "_", cfg$hydroclim$drias$scenarios)
scenarios <- rep(scenarios, each = length(rcps))
```

## Modélisation des débits de prises et de restitution.

```{r, eval=cfg$data$write_results}
WriteCRIntakeRelease <- function(rcp, scenarios) {
    if (!cfg$data$write_results) return()
  # Use future_lapply to parallelize scenario processing
  future.apply::future_lapply(scenarios, function(scenario) {
    Qnat <- readQsim(cfg$Qnat$path, cfg$Version, cfg$hydroclim$drias$source, rcp, "data", scenario)
    Q_IntakeRelease <- CreateIntakeRelease(x = Qnat,
                                           pathFolder = cfg$riskbasedrules$path,
                                           reservoirs = cfg$reservoirs$dim$id,
                                           rcp = rcp,
                                           scenario = scenario,
                                           FUN_RULES = RiskBased,
                                           RDS = TRUE,
                                           cfg = cfg)
  }, future.seed = TRUE)
}

# Set up future backend for parallel processing
future::plan(future::multisession, workers = round(future::availableCores() * 0.5))

# Call the function
WriteCRIntakeRelease(rcps, scenarios)
```

## Simulation

```{r, eval=cfg$data$write_result, warning=FALSE}
ExtractIntakesReleases <- function(rcp, scenario) {
    message("Processing ", rcp, " scenario ", scenario, "...")
    reservStates <- paste0(paste("reservoirs_states", rcp, scenario, sep = "_"), ".tsv")
    reservStates <- getDataPath(cfg$riskbasedrules, cfg$Version, cfg$hydroclim$drias$source, reservStates)

    SaveTSVReservoirsStates(
        x = readr::read_tsv(reservStates, show_col_types = FALSE),
        pathFolder = cfg$riskbasedrules$path,
        historical = FALSE,
        rcp = rcp,
        scenario = scenario
    )
}

if(cfg$data$write_results)
    mapply(ExtractIntakesReleases, rcps, scenarios)
```

```{r}
LoadRunSave <- function(rcp, scenarios) {
  # Use future_lapply to parallelize scenario processing
  future.apply::future_lapply(scenarios, function(scenario) {

    # Simulation
    file_bvObs <- paste0(paste("BasinObs", rcp, scenario, sep = "_"), ".RDS")
    BasinsObs <- loadBasinsObs(RDSFile=file.path(cfg$hydroclim$drias$source, file_bvObs),
                               TSVpath = cfg$riskbasedrules$path, cfg = cfg)

    BasinsObs$griwrm <- addReservoirsGRiwrm(BasinsObs$griwrm)
    InputsModel = suppressMessages(CreateInputsModel(BasinsObs, cfg=cfg))
    OutputsModel <- RunModel(BasinsObs, paramFile = "Calib_ErrorCrit_KGE_influenced.csv")

    # AUZON/AMANCE exception : all flow is diverted to LAC_AUBE
    attr(OutputsModel,"Qm3s")$AMANCE <- OutputsModel$AMANCE$Qnat
    attr(OutputsModel,"Qm3s")$AUZON <- OutputsModel$AUZON$Qnat

    # Sauvegarde des ts_Qsim
    path <- getDataPath(cfg$riskbasedrules$path, cfg$Version, cfg$hydroclim$drias$source, rcp, "data", scenario, cfg = cfg)
    indicators_periods <- lapply(cfg$hydroclim$drias$periods, as.POSIXct, tz = "UTC")

    saveFlowDB(path = path,
               InputsModel = InputsModel,
               OutputsModel = OutputsModel,
               indicators_periods = indicators_periods,
               cfg = cfg)

  }, future.seed = TRUE)
}

# Set up future backend for parallel processing
future::plan(future::multisession, workers = round(future::availableCores() * 0.4))
```

Il suffit ensuite d'effectuer l'opération pour tous les scénarios&nbsp;:

```{r, eval=cfg$data$write_result, warning=FALSE, message=FALSE, echo=FALSE, include=FALSE}
scenarios <- gsub("/", "_", cfg$hydroclim$drias$scenarios)
if (cfg$data$write_results)
    LoadRunSave(rcp = "rcp45", scenarios)
```

Les données de débits influencé simulés avec les forçages climatiques créées par le script ci-dessus sont téléchargeables aux adresses suivantes :

- https://nextcloud.inrae.fr/s/Mp3ynTpz3E84qiD
- https://nextcloud.inrae.fr/s/Pwg9E4FrafdSyAc

