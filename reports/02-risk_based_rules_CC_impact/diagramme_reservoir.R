s <- "
flowchart TB
l([LAKE])
i[River inlet i]
r[River outlet]
imp[Natural impluvium]
rep[Replenished river]
i -->|Natural river| r
i -->|controlled filling|l
l -->|controlled release| r
imp -->|natural filling| l
l -->|replenishment| rep
"
DiagrammeR::mermaid(s)
