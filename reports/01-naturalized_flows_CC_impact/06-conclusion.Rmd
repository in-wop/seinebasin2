# Conclusion

A semi-distributed hydrological model of the Seine River Basin has been set up.
It used the **airGRiwrm** R package which allows the integration of anthropogenic
influences in the modeled flows.
The model has been calibrated with influenced flows on more than 150 gauging stations
using observed influences on the 4 reservoirs managed by the EPTB Seine Grands Lacs.
The calibrated model shows good performances on mean flows, an slight 
underestimation of low-flows and a good performance of high-flows.

Climate projections has been used to perform both uninfluenced flow simulations and
influenced flows with modeled current rules of the reservoirs. 

The impact of climate change has been assess through hydrological indicators and 
it shows that the impact is more important for influenced flows with current management
rules on low-flow indicators.

These outputs will be used afterward in the report 
*"IN-WOP project WP3 - Seine River study case:
Risk-based management rules and assessment of fairness under climate change"*.

Uninfluenced flows will serve to compute the risk assessment on the management
of the reservoirs and influenced flows with modeled current rules will serve as
a comparison base line for the management performance assessment of the risk based
rules developed in this report.

<!-- Insert the following Rmd via child because I wasn't able to integrate it in the 
 `rmdfiles` parameter of _bookdown.yml. I don't know why...-->
```{r after_body, child="../after_body.Rmd"}
```
