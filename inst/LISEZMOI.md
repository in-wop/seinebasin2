Données de débits naturalisés
=============================

Le jeu de données de ce répertoire provient d'une extraction réalisée à l'aide du package R 'seinebasin2'.

Un rapport expliquant comment ces données ont été obtenues est disponible à l'adresse : https://in-wop.g-eau.fr/seinebasin2

Il contient les fichiers suivants:

- stations.tsv
- ts_Qsim.tsv
- ts_Qcontrib.tsv
- Q_indicators_[DDDD]-[FFFF].tsv
- Q_monthly_[DDDD]-[FFFF].tsv
- Q_monthly_5years_[DDDD]-[FFFF].tsv

Ce sont des fichiers texte avec séparateur tabulation (Tab-Separated-Values).


Description du fichier "stations.tsv"
-------------------------------------

Ce fichier contient la liste des stations hydrométriques utilisées dans le modèle 'seinebasin2' avec les colonnes suivantes:

- `CdSiteHydro`: Identifiant HYDRO2 du site tel que figurant dans [la banque hydro ](http://www.hydro.eaufrance.fr/)
- `lambert2.x`: Abscisse dans la projection Lambert II de la station hydrométrique
- `lambert2.y`: Ordonnée dans la projection Lambert II de la station hydrométrique
- `area`: Aire du bassin versant en km²
- `nom`: Nom de la station hydrométrique (*Cours d'eau* à *commune*)
- `id_aval`: Identifiant HYDRO2 de la station hydrométrique à l'aval de la station en cours
- `distance_aval`: Distance hydraulique entre la station en cours et la station aval

Les sites issus de la banque hydro ont un identifiant commençant par la lettre `H`. Les sites supplémentaires (`A_PMARNE`, `TRANN_01`, `CHAUM_07`) proviennent de stations gérées par l'EPTB Seine Grands Lacs.


Description du fichier "ts_Qsim.tsv"
------------------------------------

Ce fichier contient les séries chronologiques des débits moyens journaliers modélisés sur chacune des stations hydrométriques.

La première colonne `DatesR` contient la date de la données moyenne journalière au format 'AAAA-MM-JJ'.

Les colonnes suivantes portent le nom de la station hydrométique correspondant à la colonne `CdSiteHydro` du fichier `stations.tsv` et elles contiennent le débit moyen journalier modélisé en m<sup>3</sup>/s.


Description du fichier "ts_Qcontrib.tsv"
----------------------------------------

Ce fichier contient les séries chronologiques de la contribution moyenne journalière modélisée sur chaque bassin versant intermédiaire. Les bassins versants intermédiaires sont délimités par le bassin versant dont l'exutoire est la station hydrométrique  donnant son nom au bassin versant auquels on soustrait tous les bassins versants identifiés à l'amont.

La première colonne `DatesR` contient la date de la données moyenne journalière au format 'AAAA-MM-JJ'.

Les colonnes suivantes portent le nom de la station hydrométique correspondant à la colonne `CdSiteHydro` du fichier `stations.tsv` et elles contiennent la contribution moyenne journalière du bassin versant intermédiaire modélisée en m<sup>3</sup>/s.


Description des fichiers "Q_indicators_[DDDD]-[FFFF].tsv"
---------------------------------------------------------

Ces fichiers contiennent des indicateurs hydrologiques calculés entre l'année `[DDDD]` et l'année `[FFFF]`. Les indicateurs calculés sont les suivants (unités m³/s): 

- `QA`: débit annuel moyen
- `VCN10_2`, `VCN30_2`, `VCN10_5`, `VCN_30_5`, `VCN10_10`, `VCN30_10`: avec `VCN[k]_[Y]` le débit moyen minimum sur une durée de `k` jours de période de retour `Y` ans
- `QMNA2`, `QMNA5`, `QMNA10`: avec `QMNA[Y]` le débit moyen minimum mensuel de période de retour `Y` ans
- `QJXA2`, `QJXA10`, `QJXA20`: avec `QJXA[Y]` le débit moyen journalier maximum annuel de période de retour `Y` ans

Pour chaque indicateur, le fichier contient:

- l'indicateur calculé sur les débits observés (colonnes `[indicateur].obs`)
- l'indicateur calculé sur les débits simulés sur la période de débits observés (colonnes `[indicateur].sim`)
- le rapport entre l'indicateur calculé sur les débits simulés et celui sur les débits observés (colonnes `R-[indicateur]`)


Description des fichiers "Q_monthly_[DDDD]-[FFFF].tsv"
------------------------------------------------------

Ces fichiers contiennent les débits moyens mensuels inter-annuels calculés entre l'année `[DDDD]` et l'année `[FFFF]` (unités m³/s).

Le fichier est structuré avec une ligne par station et une colonne par mois de l'année.


Description des fichiers "Q_monthly_5years_[DDDD]-[FFFF].tsv"
-------------------------------------------------------------

Ces fichiers contiennent les débits moyens mensuels inter-annuels quiquennaux secs calculés entre l'année `[DDDD]` et l'année `[FFFF]` (unités m³/s).

Le fichier est structuré avec une ligne par station et une colonne par mois de l'année.
