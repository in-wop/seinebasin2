
# seinebasin2

<!-- badges: start -->
<!-- badges: end -->

The goal of seinebasin2 is to modelling the Seine River basin with the
airGRiwrm package.

It has been developed to produce the studies described in these reports:

[<img src = man/figures/rapport_icon.png width  = "35"
              height = "35"
              align = "center"
            > IN-WOP project WP3 - Seine River study case: Climate
change impacts on water
resources](https://in-wop.g-eau.fr/seinebasin2/reports/01-naturalized_flows_CC_impact)

[<img src = man/figures/rapport_icon.png width  = "35"
              height = "35"
              align = "center"
            > IN-WOP project WP3 - Seine River study case: Risk-based
management rules and assessment of fairness under climate
change](https://in-wop.g-eau.fr/seinebasin2/reports/02-risk_based_rules_CC_impact)

These reports are made of *Rmarkdown* documents which compute almost all
the results of the study.

## Installation

### Package only

For only using the functions available in *seinebasin2* package, you can
install it with the following instructions:

``` r
install.packages("remotes")
remotes::install_git("https://forgemia.inra.fr/in-wop/seinebasin2.git", dependencies = TRUE)
```

### The whole source code for render the reports

If you need to modify or render the reports, you need to clone the
repository whose URL is
<https://forgemia.inra.fr/in-wop/seinebasin2.git>. You can do it an IDE
or by typing the following in a terminal:

    git clone https://forgemia.inra.fr/in-wop/seinebasin2.git

Then you can open the Rstudio project (File `seinebasin2.Rproj`) and
type the following instructions for finishing the installation:

``` r
install.packages("devtools") # If it's not already installed
devtools::install_deps(dependencies = TRUE)
devtools::install()
```

## Package documentation

See the “Reference” section for a documentation of the functions
included in this package.

<div style="display: flex; justify-content: space-between;">

<img src="man/figures/logo_water_jpi.png" alt="Water JPI"/>
<img src="man/figures/logo_water_works_2017.png" alt="Water Works 2017"/>
<img src="man/figures/logo_european_commission.jpg" alt="European Commission"/>
<img src="man/figures/logo_2018_joint_call.png" alt="2018 Joint call"/>

</div>

## Rendering the reports

The reports are based on the package *fairify* which allows to render a
website containing the package documentation and the reports in both
format html and pdf. The website and the reports are rendered in the
`public` subfolder. From the root folder of the package you can type:

``` r
# Build the entire website
fairify::build_site(allow_duplicate_labels = TRUE)
# Render reports only
fairify::render_reports(allow_duplicate_labels = TRUE)
```

## Known issues

Getting error message *“pandoc version 1.12.3 or higher is required and
was not found”* when rendering the reports outside of Rstudio.

Try the solution proposed by
<https://stackoverflow.com/questions/28432607/pandoc-version-1-12-3-or-higher-is-required-and-was-not-found-r-shiny>
