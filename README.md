# seinebasin2

<!-- badges: start -->
<!-- badges: end -->

The goal of seinebasin2 is to modelling the Seine River basin with the airGRiwrm package.

It has been developed to produce the studies described in this report: https://in-wop.g-eau.fr/seinebasin2/

## Installation

You can install the development version of seinebasin2 like so:

``` r
install.packages("remotes")
remotes::install_gitlab("in-wop/seinebasin2", host = "gitlab.irstea.fr", dependencies = TRUE)
```

## Launch the library

``` r
library(seinebasin2)
```

## Documentation

See the "Reference" section for a documentation of the functions included in this package.
