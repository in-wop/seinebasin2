#' Select a period in a time series
#'
#' @details
#' Select time steps corresponding to one of the DRIAS 2020 periods:
#' "ref" for reference (1976-2005), "near" for near future (2021-2050),
#' "middle" for middle of the century (2041-2070), "end" for end of the century (2071-2100).
#'
#' @param df [data.frame] with a first column of [POSIXt] dates and [numeric] in the other columns
#' @param period [character], see details
#' @template param_cfg
#'
#' @return [data.frame] with a first column of [POSIXt] dates and [numeric] in the other columns with selected rows according to the chosen period
#' @export
#'
selectDriasPeriod <- function(df, period, cfg = loadConfig()) {
    periods <- lapply(cfg$hydroclim$drias$periods, as.POSIXct, tz = "UTC")
    if (!period %in% names(periods)) {
        stop("`period`should be in ",
             paste(sprintf("\"%s\"", names(periods)), collapse = ", "))
    }
    selectDates <- df$DatesR >= periods[[period]][1] &
        df$DatesR <= periods[[period]][2]
    return(df[selectDates, ])
}
