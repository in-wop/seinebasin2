#' Create Intake and Release flows
#'
#' @description
#' That function create intake and release flows, from simulated naturalized flows
#' (see, SeineBasin2::RunModel)
#'
#' @inheritParams CurrentRules
#' @param pathFolder location for replenishment & log
#' @param ... Parameters sent to `FUN_RULES`
#'
#' @importFrom lubridate day year ymd month
#' @importFrom xts xts
#' @importFrom stats setNames
#' @importFrom rvgest getObjectiveStorageCurves
#'
#' @return [data.frame] with intake and release flows
#' @export
#'
CreateIntakeRelease <- function(x,
                                pathFolder,
                                reservoirs=cfg$reservoirs$dim$id,
                                seqDates = as.Date(x$DatesR),
                                FUN_RULES = CurrentRules,
                                historical = FALSE,
                                rcp,
                                scenario,
                                ...,
                                saveOutput = TRUE,
                                cfg=loadConfig()
                                ){
    if (cfg$data$write_results) {
        IntakeRelease <-
            FUN_RULES(x, pathFolder, reservoirs, seqDates, historical, rcp,
                      scenario, ..., saveOutput, cfg)

        return(IntakeRelease)
    }
}

#' Save TSV files reservoirs states
#'
#' @description prepares the intake and release data used by the loadBasinsObs
#' function
#'
#' @param x reservoirs states files (see : `RiskBased`)
#' @param path location for reservoirs states
#' @param historical [logical] set on true if `x` contain historical data
#'
#' @import dplyr
#'
#' @return tsv file usable for the loadBasinObs function
#' @export
#'
SaveTSVReservoirsStates <- function(x,
                                    pathFolder,
                                    reservoirs = cfg$reservoirs$dim$id,
                                    seqDates = as.Date(x$date),
                                    historical = FALSE,
                                    rcp,
                                    scenario,
                                    ...,
                                    cfg = loadConfig()) {

    if (!cfg$data$write_results) return()

    col_used <- c(reservoirs, cfg$reservoirs$diversions$id)
    x <- data.frame(date = x[,1], x[which(colnames(x) %in% col_used)])

    # remove any duplicates
    x <- x %>% arrange(x[,1])

    if(any(is.na(x$date))){
        x <- x[-which(is.na(x$date)),]
    }

    x <- subset(x, !duplicated(x[,1]))

    # find missing dates
    all_dates <- as.POSIXct(seq.Date(as.Date(first(x$date)), as.Date(last(x$date)), 1))
    x_dates <- x$date

    missing_dates <- which(is.na(match(all_dates, x_dates)))
    missing_dates <- all_dates[missing_dates]

    # add missing dates with previus dates values
    for (date in seq_along(missing_dates)) {
        previous_row <- x[which(x$date == missing_dates[date]-1), ]
        new_row <- previous_row
        new_row$date <- missing_dates[date]
        x <- rbind(x, new_row)
        tail(x)
    }
    x <- x[order(x$date), ]

    # remove empty lines
    if(any(is.na(x[,1]))){
        x <- x[-which(is.na(x[,1])),]
    }

    # import replenishment
    repla_reserv <- unique(cfg$reservoirs$resupplies$id_lac)

    Qresup <-
        do.call(cbind, lapply(repla_reserv, function(lake) {
            getResuppliesTS(
                DatesR = x[,1],
                path = pathFolder,
                reservoir = lake,
                historical = historical,
                cfg = cfg
            )
        }))

    coln_resup <- gsub(".*\\.", "", colnames(Qresup))
    coln_resup[coln_resup %in% c("DROYES", "NIVERNAIS")] <- c("R_MARNE",
                                                              "H2021010")
    colnames(Qresup) <- coln_resup

    if (isTRUE(historical)) {
        if(as.Date(tail(seqDates, 1)) > as.Date("1988-03-04")){
            match_date <- seqDates %in% seq.Date(as.Date("1988-03-04"),
                                                 as.Date(tail(seqDates, 1)), 1)
            if (any(match_date)) {
                Qresup[match_date, c("AMANCE", "AUZON")] <- 1000
                Qresup[!match_date, "R_AUBE"] <- 0
            }
        }
        Qresup$R_AUBE <- -Qresup$AUZON / 86400
    } else{
        Qresup[, c("AMANCE", "AUZON")] <- 1000
        Qresup$R_AUBE <- 0
    }

    # merge flows and replenishments
    x <- x[x[,1] %in% Qresup$Dates,]
    Qresup <- Qresup[-grep("Dates", colnames(Qresup))]

    x <- data.frame(dates = x[,1], round(x[,-1]/86400, digits=2), Qresup)

    # set intakes to negative
    x[,cfg$reservoirs$diversions$id] <- -x[,cfg$reservoirs$diversions$id]

    # saving tsv files
    if (isTRUE(historical)) {
        file_name <- "IntakesReleases.tsv"
        readr::write_tsv(x, file.path(
            getDataPath(pathFolder, cfg$Version, "historical"),
            file_name
        ))

    } else {
        file_name <- paste0("IntakesReleases_", rcp, "_", scenario, ".tsv")
        readr::write_tsv(x, file.path(
            getDataPath(pathFolder, cfg$Version, cfg$hydroclim$drias$source),
            file_name
        ))
    }

    return(x)
}



