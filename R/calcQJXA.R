#' QJXA values
#'
#' Calculation of Maximum daily flow per year.
#'
#' @param x [POSIXt] dates, or [data.frame] with a first column with dates, or \code{OutputsModel} produced by [airGR::RunModel]
#' @param threshold [numeric] minimum ratio of available data in a year to take it into account
#' @param flows [numeric] flows in l/s for use with x as a [POSIXt]
#' @param ... for S3 method compability
#'
#' @importFrom stats aggregate
#' @return A [vector] of [numeric] for x [POSIXt] or \code{OutputsModel} and a [data.frame] for x [data.frame]
#'
#' @rdname calcQJXA
#' @export
#'
#' @examples
#'#! load data
#'data(L0123001, package = "airGR")
#'
#' #! define vector of flows, time step is the day
#' flows <- BasinObs$Qls
#'
#' #! define vector of dates
#' dates <- BasinObs$DatesR
#'
#'#! --- example with two vectors for the dates and the flows
#'
#' #! define hydrological years
#' QJXA <- calcQJXA(x = dates, flows = flows)
#'
#' #! --- example with the output of the function "RunModel" in the package airGR.
#' #! --- QJXA is calculated on simulated flows.
#'
#' ## preparation of the InputsModel object
#' InputsModel <- airGR::CreateInputsModel(FUN_MOD = airGR::RunModel_GR4J,
#'                                  DatesR = BasinObs$DatesR,
#'                                  Precip = BasinObs$P,
#'                                  PotEvap = BasinObs$E)
#'
#' ## run period selection
#' Ind_Run <- seq(which(format(BasinObs$DatesR,
#'                                 format = "%d/%m/%Y") == "01/01/1990"),
#'                which(format(BasinObs$DatesR,
#'                                 format = "%d/%m/%Y") == "31/12/1999"))
#'
#' ## preparation of the RunOptions object
#' RunOptions <- airGR::CreateRunOptions(FUN_MOD = airGR::RunModel_GR4J,
#'                                InputsModel = InputsModel,
#'                                IndPeriod_Run = Ind_Run)
#'
#' ## simulation
#' Param <- c(257.238, 1.012, 88.235, 2.208)
#' OutputsModel <- airGR::RunModel_GR4J(InputsModel = InputsModel,
#'                                RunOptions = RunOptions,
#'                                Param = Param)
#'
#' QJXA <- calcQJXA(x = OutputsModel)
#'
calcQJXA <- function(x, ...) {
  UseMethod("calcQJXA")
}


#' @export
#' @rdname calcQJXA
calcQJXA.data.frame <- function(x, threshold = 0.8, ...) {
    # Calculate monthly mean flow
    QD <- suppressWarnings(
        SeriesAggreg(x, Format = "%Y%m%d", rep("mean_na_rm", ncol(x)-1))
    )

    # Calculate maximum of daily mean flow
    QJXA <- SeriesAggreg(QD, Format = "%Y", rep("max_na_rm", ncol(x)-1))
    nDaysNotNA <- SeriesAggreg(QD, Format = "%Y", rep("not_na_count", ncol(x)-1))
    QJXA <- QJXA[, -1, drop = FALSE]
    nDaysNotNA <- nDaysNotNA[, -1, drop = FALSE]
    QJXA[nDaysNotNA < threshold * 365] <- NA
    return(QJXA)
}

#' @rdname calcQJXA
#' @export
calcQJXA.OutputsModel <-
  function(x, ...) {
    calcQJXA(
      x = x$DatesR,
      flows = x$Qsim
    )
  }


#' @rdname calcQJXA
#' @export
calcQJXA.POSIXt <-
  function(x, flows, ...) {
    dates <- x

    ##_____Arguments_check________________________________________________________

    if (!is.numeric(flows)) {
      stop("flows must be of class 'numeric'")
    }

    if (!inherits(dates, "POSIXt")) {
      stop("dates must be of class 'POSIXt'")
    }

    if (length(dates) !=  length(flows)) {
      stop("dates and flows must have the same length")
    }

    #! we select dates and Q where Q is not NA
    i_no_na <- !is.na(flows)
    dates_no_na <- dates[i_no_na]
    flows_no_na <- flows[i_no_na]

    if (length(flows_no_na) <= 1) {
      stop("flows must have more than one element with value not equal to NA")
    }

    years <- as.integer(format(dates_no_na, "%Y"))

    years_unique <- unique(years)
    n_year <- length(years_unique)

    months <- as.integer(format(dates_no_na, "%m"))
    days <- as.integer(format(dates_no_na, "%j"))

    names_col <- c("QJXA")
    n_col <- length(names_col)
    tab_QJXA <- data.frame(matrix(
      NA,
      nrow = n_year,
      ncol = n_col,
      dimnames = list(NULL, names_col)
    ))

    for (i_year in seq_len(n_year)) {
      year <- years_unique[i_year]
      ind <- years == year

      if (length(unique(months[ind])) == 12) {
        tmp_Q <- aggregate(
          flows_no_na[ind],
          by = list(days[ind]),
          FUN = mean,
          na.rm = TRUE
        )

        tab_QJXA[i_year, "QJXA"] <- max(tmp_Q$x)

      }
    }


    QJXA <- tab_QJXA$QJXA

    QJXA <- QJXA[!is.na(QJXA)]
    class(QJXA) <- c("QJXA", class(QJXA))

    return(QJXA)
  }


#' Compute Gumbel's laws parameters
#'
#' @param x Maximum flow at a given time step (usually the maximum daily flow per year)
#'
#' @return A [list] with `a` the location parameter and `b` the scale parameter
#' @importFrom stats sd
#' @export
#'
#' @examples
#'#! load data
#'data(L0123001, package = "airGR")
#'
#'#! define vector of flows, time step is the day
#'flows <- BasinObs$Qls
#'
#'#! define vector of dates
#'dates <- BasinObs$DatesR
#'
#'#! define return period
#'pRet <- 20
#'
#' QJXA <- calcQJXA(x = dates, flows = flows)
#' gumbel <- getGumbelParams(QJXA)
#'
getGumbelParams <- function(x) {
  ##_____Arguments_check________________________________________________________

  if (!is.numeric(x)) {
    stop("maxFlows must be of class 'numeric'")
  }

  #! Euler constant
  c_euler <- 0.5772

  #! scale parameter
  b <- sqrt(6) / pi * sd(x, na.rm = TRUE)

  #! location parameter
  a <- mean(x, na.rm = TRUE) - b * c_euler

  GumbelParams <- list(a = a, b = b)
  class(GumbelParams) <- c("GumbelParams", class(GumbelParams))
  return(GumbelParams)
}


#' Yearly maximum daily flow for a return period
#'
#' @description
#' The maximum daily flow is calculated from an adjustement on a Gumbel law.
#'
#' Functions named calcQJXA_\[return_period\] are also defined for commun indicators (2, 10, 20 years).
#'
#' @param x [data.frame] of flows (the first column should be a [POSIXct]), or a `QJXA` object (See [calcQJXA])
#' @param return_period [numeric] Return period. Its unit is given by the aggregation time step used for the Gumbel law.
#'
#' @return A [numeric] with the calculated flow
#' @export
#' @rdname calcQJXAn
#'
#' @examples
#' #! load data
#' data(L0123001, package = "airGR")
#'
#' #! define return period
#' return_period <- 10
#'
#' # Calculation in detail
#' QJXA <- calcQJXA(x = BasinObs$DatesR, flows = BasinObs$Qls / 1000)
#' gumbel <- getGumbelParams(QJXA)
#' calcQJXAn(gumbel, return_period)
#'
#' # Or directly
#' dfQ <- data.frame(DatesR = BasinObs$DatesR, Q = BasinObs$Qls / 1000)
#' calcQJXAn(dfQ, return_period)
#'
calcQJXAn <- function(x, return_period) {
  UseMethod("calcQJXAn", x)
}


#' @export
#' @rdname calcQJXAn
calcQJXAn.QJXA <- function(x, return_period) {
    calcQJXAn(getGumbelParams(x), return_period)
}

#' @rdname calcQJXAn
#' @export
calcQJXAn.data.frame <- function(x, return_period) {
    QJXA <- calcQJXA(x)
    apply(QJXA, 2, calcQJXAn.QJXA, return_period = return_period)
}


#' @export
#' @rdname calcQJXAn
calcQJXAn.GumbelParams <- function(x, return_period) {

  if (!is.numeric(return_period)) {
    stop("return_period must be of class 'numeric'")
  }

  if (return_period <= 1) {
    stop("return_period must be greater than 1")
  }

  #! non-exceeding frequency
  F_x <- 1 - (1 / return_period)

  #! reduced gumbel variable
  u <- -log(-log(F_x))

  return(x$b * u + x$a)

}

#' Maximum with NA stripped before computation
#'
#' @param x An R object.
#'
#' @return See [max]
#' @export
#'
#' @examples
#' max_na_rm(c(1, 2, 3, NA, 4))
#'
max_na_rm <- function(x) {
    suppressWarnings(max(x, na.rm = TRUE))
}


#' @export
#' @rdname calcQJXAn
calcQJXA2 <- function(x) calcQJXAn(x, 2)

#' @export
#' @rdname calcQJXAn
calcQJXA10 <- function(x) calcQJXAn(x, 10)

#' @export
#' @rdname calcQJXAn
calcQJXA20 <- function(x) calcQJXAn(x, 20)

