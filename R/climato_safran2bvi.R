#' Convert a data from a SAFRAn grid to BVI scale
#'
#' @param safran data in SAFRAn format
#' @param dfMailles correspondence between SAFRAN grid and BVIs
#'
#' @return [matrix]
#'
climato_safran2bvi <- function(safran, dfMailles) {
    dfMailles$id_safran <- paste(dfMailles$x, dfMailles$y, sep = "x")
    bvi_ids <- unique(dfMailles$CODE)
    names(bvi_ids) <- bvi_ids
    l <- lapply(bvi_ids, climato_safran2bvi1, safran = safran, dfMailles = dfMailles)
    return(do.call(cbind, l))
}

climato_safran2bvi1 <- function(id, safran, dfMailles) {
    mailles_bvi <- dfMailles[dfMailles$CODE == id,]
    data <- 0
    for(i in seq_len(nrow(mailles_bvi))) {
        id_safran <- mailles_bvi$id_safran[i]
        data <- data + safran$cells[[id_safran]]$data * mailles_bvi$prop[i]
    }
    return(data)
}
