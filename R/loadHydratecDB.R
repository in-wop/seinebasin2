#' Load Hydratec naturalised flow database
#'
#' @template param_cfg
#'
#' @source Hydratec. 2011. Actualisation de la base de données des débits journaliers ‘naturalisés’ - Phase 2. 26895-LME/TL.
#'
#' @return A [list] with 2 items:
#'
#' - `stations`: a [data.frame] with the description of gauging stations
#' - `Q`: the times series of daily flows
#' @export
#'
loadHydratecDB <- function(cfg = loadConfig()) {
    # Load time series
    file <- getDataPath(cfg$hydroclim$path, "EPTB_SGL/Qnat/Hydratec/Q_NAT_1900-2009.txt", cfg = cfg)
    Qhydratec <- read.csv(file = file, sep = "\t")
    Qhydratec$Dates <- as.POSIXct(lubridate::ymd(Qhydratec$Dates))
    # Fill 29th February with interpolated values
    Qhydratec <- tidyr::complete(Qhydratec, Dates = seq(min(Qhydratec$Dates), max(Qhydratec$Dates), by = "1 day"))
    Qhydratec <- cbind(
        data.frame(DatesR = Qhydratec$Dates),
        apply(Qhydratec[, -1], 2, zoo::na.approx, x = Qhydratec$Dates, na.rm = FALSE)
    )
    # Load station description
    file <- getDataPath(cfg$hydroclim$path, "EPTB_SGL/Qnat/Hydratec/01_Liste_BV.txt", cfg = cfg)
    stationsHydratec <- read.csv(file = file, sep = ";", encoding = "UTF-8")
    # For stations not in Hydro2
    emptyCdSiteHydro <- stationsHydratec$CdSiteHydro == ""
    stationsHydratec$CdSiteHydro[emptyCdSiteHydro] <- stationsHydratec$CODE_SGL[emptyCdSiteHydro]
    # Keep only stations in the model
    data("griwrm", envir = environment())
    stationsHydratec <- stationsHydratec[stationsHydratec$CdSiteHydro %in% griwrm$id, ]
    # Apply selection to time series
    stationsHydratec$CODE_SGL <- gsub("-", ".", stationsHydratec$CODE_SGL) # issue with column names containing "-"
    Qhydratec <- cbind(DatesR = Qhydratec$DatesR, Qhydratec[, names(Qhydratec) %in% stationsHydratec$CODE_SGL])
    Qhydratec <- Qhydratec[, c("DatesR", stationsHydratec$CODE_SGL)]
    names(Qhydratec) <- c("DatesR", stationsHydratec$CdSiteHydro)

    return(list(stations = stationsHydratec, Q = Qhydratec))
}
