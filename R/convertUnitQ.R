#' Convert flows units
#'
#' @param x a [data.frame] with a first column with the time stamp in [POSIXct] format and folling columns with flows
#' @param ...
#'
#' @return [data.frame]  with a first column with the time stamp in [POSIXct] format and folling columns with converted flows
#'
#' @rdname ConvertUnitQ
#' @export
#'
#' @examples
#' data("L0123001", package = "airGR")
#' dfQmm <- data.frame(DatesR = BasinObs$DatesR, L0123001 = BasinObs$Qmm)
#' dfQls <- convertUnitQ(dfQmm, c(L0123001 = BasinInfo$BasinArea), from = "mm", to = "l/s")
#' head(dfQls)
#' head(BasinObs$Qls)
convertUnitQ <- function(x, ...) {
    UseMethod("convertUnitQ", x)
}


#' @rdname ConvertUnitQ
#' @param areas named [vector], basin areas in km², `names` must correspond to station ids in columns names of `x`
#' @param from flow unit of `x`, see details
#' @param to flow unit of returned values
#'
#' @details
#' Available units are:
#'
#' - "mm" for mm per time step
#' - "m3" for m3 per time step
#' - "m3/s" for m3 per second
#' - "l/s" for liter per second
#'
#' The time step is determined with the time difference between rows in `x`.
#'
#' @export
convertUnitQ.data.frame <- function(x, areas, from, to, ...) {
    ids <- names(x)[-1]
    areas <- areas[ids]
    time_step <- as.numeric(x[2,1] - x[1,1], units = "secs")
    # Remove "/" from unit
    from <- gsub("/", "", from)
    to <- gsub("/", "", to)
    # Factor to convert from m3/time step to...
    factors <- list(mm = 1E-3 / areas,
                    m3 = 1,
                    m3s = 1 / time_step,
                    ls = 1000 / time_step)
    out <- apply(x[, -1, drop = FALSE], 1, function(row) {
        row  * factors[[to]] / factors[[from]]
    })
    if (is.matrix(out)) {
        out <- t(out)
    } else {
        out <- matrix(out, ncol = 1)
        colnames(out) <- ids
    }
    cbind(DatesR = x[, 1], as.data.frame(out))
}
