#' Extract the names of the files concerned by a GCM/RCM scenario
#'
#' @param scenario [character] Scenario name in format "GCM/RCM"
#' @param scenario_data_files [character] Names of DRIAS 2020 netCDF files
#'
#' @return A [character] vector of selected DRIAS 2020 files
#' @export
#'
#' @examples
#' \dontrun{
#' cfg <- loadConfig()
#' # The following operation as it download the netcdf files takes
#' # a very, very long time on the cloud
#' driasFiles <- list.files(getDataPath(cfg$hydroclim$path, "drias",
#' cfg$hydroclim$drias$source, cfg = cfg))
#' getDrias2020filenames(cfg$hydroclim$drias$scenarios[1], driasFiles[1],
#' cfg = cfg)
#' }
getDrias2020filenames <- function(scenario, scenario_data_files) {
    scenario <- strsplit(scenario, "/")[[1]]
    l <- lapply(scenario, function(x) {
        s <- strsplit(x, "-")[[1]]
        s[1:min(length(s), 2)]
    })
    files <- scenario_data_files
    for (i in seq_along(l)) {
        for (x in l[[i]]) {
            files <- files[grep(x, files)]
        }
    }
    return(files)
}
