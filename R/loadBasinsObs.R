#' Load observations for the semi-distributed model
#'
#' @param RDataFile path of the file inside `cfg$hydroclim$path`
#' @param griwrm GRiwrm object describing the semi-distributed network (default `data("griwrm")`, see [griwrm])
#' @template param_cfg
#'
#' @return A `BasinsObs` object (See [createBasinsObs])
#' @export
#'
#' @examples
#' BasinsObs <- loadBasinsObs("BasinsObs_observations_day_1958-2019.RDS")
#' str(BasinsObs)
#'
loadBasinsObs <- function(RDataFile, griwrm = NULL, cfg = loadConfig()) {

    file <- getDataPath(cfg$hydroclim$path, RDataFile, cfg = cfg)

    if (is.null(griwrm)) {
        data("griwrm", envir = environment())
    }

    BasinsObs <- readRDS(file = file)
    BasinsObs$griwrm <- griwrm
    BasinsObs$DatesR <- as.POSIXct(BasinsObs$DatesR)
    BasinsObs$HypsoData <- loadHypsoData(cfg)

    class(BasinsObs) <- c("BasinsObs", class(BasinsObs))
    return(BasinsObs)
}
