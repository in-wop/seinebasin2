#' Median value
#'
#' @description
#' Returns the (regular or parallel) median of the input values.
#'
#' `pmedian`  takes one or more vectors as arguments which should have same dimensions.
#'
#' @param ...  Several [matrix] of same dimensions
#' @importFrom stats median
#'
#' @return a [matrix] of same dimension as the ones in `...` with median value calculated for each cell.
#' @export
#'
pmedian <- function(...) {
    l <- list(...)
    m <- sapply(seq.int(length(l[[1]])), function(i) {
        cells <- sapply(l, function(x) x[i])
        median(cells)
    })
    m <- matrix(m, ncol = ncol(l[[1]]))
    colnames(m) <- colnames(l[[1]])
    rownames(m) <- rownames(l[[1]])
    return(m)
}
