# seinebasin2 development version

* Modelling of the reservoirs with the current rules #22

# seinebasin2 0.3.0 - 2023-03-27

* Ajouter les points de prélèvements et de restitution des réservoirs au modèle #28
* Calibration of network v2.0 #30

# seinebasin2 0.2.1 - 2023-03-14

* Added a `NEWS.md` file to track changes to the package.
* Use of DRIAS2020 projection with ADAMONT correction instead of R2D2
* Potential bug in 02-Calage in calculation of hydrological indicators on calibration period #37
* Remove all references to gitlab.irstea.fr #38
* Move website to gitlab pages #39


# seinebasin2 0.2.0 - 2023-01-06

* Find and correct vignet04.rmd -> knit pb	#19
* Modelling naturalised flows for all DRIAS climate projections	#20
* Reorganize the outputs of the package	#21
* Add CI test on bookdown build and debug Rmd 05 and others	#24
* Déplacer Qnat "Historical" dans le dossier 03-naturalised_flows/v1.0/historical	#31
